package com.learn;

import redis.clients.jedis.Jedis;

public class App {
    public static void main(String[] args) {
        App app=new App();
        app.testSet("key","sno1");

        final String val = app.testGet("test");
        System.out.println(val);
}

    public void testSet(String key,String val){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.set(key,val);
        }finally {
            resource.close();
        }
    }

    public String testGet(String key){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            final String val = resource.get(key);
            return val;
        }finally {
            resource.close();
        }
    }

    //实现对key：sno1的删除
    public void testDelete(String key){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            final String val = resource.get(key);
            resource.del(key) ;
        }finally {
            resource.close();
        }
    }
}


